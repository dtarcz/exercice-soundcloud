<div class="body2">

  <div class="container">
    <div class="trait">
    </div>

    <img class="iphone" src="assets/img/iphone.png" alt="image iphone" />

    <div class="paragraphe">
        <h1>Get the app</h1>
        <p class="phrase">Never stop listening. Take your playlists and likes wherever you go.</p>
    </div>


    <div class="download">
      <img class="appstore" src="assets/img/logo_appstore.png" alt="image iphone" />
      <img class="googleplay" src="assets/img/logo_googleplay.png" alt="image iphone" />
    </div>

    <div class="learn">
      <p class="learn_more">or <a  class="more" href="#">Learn more</a></p>
    </div>

    <div class="banner">
      <img class="degrader" src="assets/img/degrader.png" alt="images degrader" />
      <img class="dj" src="assets/img/dj.png" alt="images dj" />
    </div>

    <div class="texte">
      <h1 class="titre2">Make music? Create audio?</h1>
      <p class="text2">Get <span>ON SOUNDCLOUD</span> to help you connect with fans and grow your audience.</p>
    </div>

    <div class="boutton_body2">
      <button class="find_out_more" type="button" name="button">Find out more</button>
    </div>

  </div>

</div>
