<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>SoundCloud</title>
    <link rel="stylesheet" href="assets/css/master.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>

    <?php include 'header.php'; ?>

    <?php include 'body1.php'; ?>

    <?php include 'body2.php'; ?>

    <?php include 'footer.php'; ?>

  </body>
</html>
