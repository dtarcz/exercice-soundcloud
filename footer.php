
<div class="footer">

	<h1 class="titre">Thanks for listening. Now join in.</h1>

	<p class="titre2-footer">classSave tracks, follow artists and build playlists. All for free.</p><br/>

		<p class="compte"><button class="bouton boutonCompte" tabindex="0" title="Create a SoundCloud account">Create account</button></p>

	<br/>

	<p class="loginCompte">Already have an account?
		<button class="bouton boutonLogin" tabindex="0" title="Sign in">Sign in</button>
	</p>

</div>
<br/>

<hr noshade width="1500" size="1">

<div class="footerLink">

	<p>
		<a class="link" title="Popular searches" href="/popular/searches">Popular searches</a>
		<a class="link" title="People directory" href="/people/directory">Directory</a>
		<a class="link" title="About SoundCloud" href="/pages/contact">About us</a>
		<a class="link" title="SoundCloud blog" target="_blank" href="http://blog.soundcloud.com">Blog</a>
		<a class="link" title="Jobs at SoundCloud" target="_blank" href="http://soundcloud.com/jobs">Jobs</a>
		<a class="link" title="SoundCloud for developers" target="_blank" href="http://developers.soundcloud.com">Developers</a>
		<a class="link" title="SoundCloud help" target="_blank" href="http://help.soundcloud.com">Help</a>
		<a class="link" title="Privacy policy" href="/pages/privacy">Privacy</a>
		<a class="link" title="Cookies policy" href="/pages/cookies">Cookies</a>
		<a class="link" title="Company information" href="/imprint">Imprint</a>
	</p>
</div>
