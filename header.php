<div class="header">
  <div class="header-logo">
    <p>
      SOUNDCLOUD
    </p>
  </div>
  <div class="login">
    <p class="signin">
      Sign in
    </p>
    <p class="create">
      Create account
    </p>
  </div>
  <div class="header-titre">
    <p>
      Find the music you love. Discover new tracks. <br>
      Connect directly with your favorite artists.
    </p>
  </div>
</div>
